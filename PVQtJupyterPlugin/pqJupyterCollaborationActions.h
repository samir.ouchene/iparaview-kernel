#ifndef pqJupyterCollaborationActions_h
#define pqJupyterCollaborationActions_h

#include <QActionGroup>

class pqJupyterCollaborationActions : public QActionGroup
{
  Q_OBJECT
public:
  pqJupyterCollaborationActions(QObject* p);
  virtual ~pqJupyterCollaborationActions() = default;
};

#endif
