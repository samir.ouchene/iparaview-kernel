
#include "pqJupyterTraceReaction.h"

#include <pqActiveObjects.h>
#include <pqApplicationCore.h>
#include <pqCollaborationManager.h>
#include <pqCoreUtilities.h>
#include <pqPVApplicationCore.h>
#include <pqPythonScriptEditor.h>
#include <pqServer.h>

#include <QApplication>
#include <QMainWindow>
#include <QMenuBar>
#include <QStyle>

#include <vtkSMCollaborationManager.h>
#include <vtkSMSession.h>
#include <vtkSMTrace.h>

//-----------------------------------------------------------------------------
class pqJupyterTraceReaction::pqInternals
{
public:
  QPointer<QAction> StandardTraceAction = nullptr;
  QString ExtraLines;

  pqInternals() = default;

  void findStandardTraceAction()
  {
    if (!this->StandardTraceAction)
    {
      QMainWindow* mainWindow = qobject_cast<QMainWindow*>(pqCoreUtilities::mainWidget());
      auto listActions = mainWindow->menuBar()->findChildren<QAction*>("actionToolsStartStopTrace");
      if (listActions.size() >= 1)
      {
        this->StandardTraceAction = listActions[0];
      }
    }
  }
};

//-----------------------------------------------------------------------------
pqJupyterTraceReaction::pqJupyterTraceReaction(QAction* p)
  : Superclass(p)
  , Internals(new pqInternals)
{
  this->initialize();

  if (!this->Internals->StandardTraceAction)
  {
    auto coreApp = pqApplicationCore::instance();
    QObject::connect(coreApp, &pqApplicationCore::clientEnvironmentDone, this,
      &pqJupyterTraceReaction::initialize);
  }
}

// Cannot be '= default' because member QScopedPointer<pqInternals> uses forward declared
// pqInternals. see https://doc.qt.io/qt-5/qscopedpointer.html#forward-declared-pointers
//-----------------------------------------------------------------------------
pqJupyterTraceReaction::~pqJupyterTraceReaction() {}

//-----------------------------------------------------------------------------
void pqJupyterTraceReaction::initialize()
{
  this->Internals->findStandardTraceAction();

  if (this->Internals->StandardTraceAction)
  {
    // watch changed signal and not triggered to be sure the reaction already handled the trigger.
    this->connect(this->Internals->StandardTraceAction, &QAction::triggered, this,
      &pqJupyterTraceReaction::onStandardAction);
  }

  this->updateAction();
}

//-----------------------------------------------------------------------------
void pqJupyterTraceReaction::onStandardAction()
{
  auto action = this->parentAction();
  bool traceStarted = vtkSMTrace::GetActiveTracer() != nullptr;
  action->setEnabled(!traceStarted);
}

//-----------------------------------------------------------------------------
void pqJupyterTraceReaction::updateAction()
{
  auto action = this->parentAction();
  bool traceStarted = vtkSMTrace::GetActiveTracer() != nullptr;
  if (traceStarted)
  {
    QIcon icon = qApp->style()->standardIcon(QStyle::SP_DialogYesButton);
    action->setIcon(icon);
    action->setToolTip("Send Trace to Notebook");
  }
  else
  {
    QIcon icon = qApp->style()->standardIcon(QStyle::SP_DialogNoButton);
    action->setIcon(icon);
    action->setToolTip("Record Trace");
  }
}

//-----------------------------------------------------------------------------
void pqJupyterTraceReaction::onTriggered()
{
  auto tracer = vtkSMTrace::GetActiveTracer();
  this->Internals->StandardTraceAction->setEnabled(tracer);
  pqServer* server = pqApplicationCore::instance()->getActiveServer();
  auto* manager = server ? server->session()->GetCollaborationManager() : nullptr;

  // if tracer already exists stop the trace
  if (manager && tracer)
  {
    auto trace = vtkSMTrace::StopTrace();
    QString traceString(trace.c_str());
    traceString.remove(this->Internals->ExtraLines);
    QStringList traceLines = traceString.split("\n");
    traceString.clear();

    traceString.append("## Traced from ParaView\n");
    for (const auto& line : traceLines)
    {
      const QString& trimmed = line.trimmed();
      // ignore additional informations
      if (trimmed.startsWith("# addendum"))
      {
        break;
      }
      else if (!trimmed.isEmpty())
      {
        traceString.append(trimmed + "\n");
      }
    }

    manager->PromoteToMaster(JUPYTER_CLIENT_ID);
    if (!traceLines.isEmpty())
    {
      this->sendToJupyter(traceString);
    }

    // Open the python editor
    pqPythonScriptEditor* editor = pqPythonScriptEditor::getUniqueInstance();
    editor->setPythonManager(pqPVApplicationCore::instance()->pythonManager());
    editor->show();
    editor->stopTrace(traceString);
    editor->raise();
    editor->activateWindow();
  }
  // else if tracer is null start the trace
  else if (manager && !tracer)
  {
    manager->PromoteToMaster(manager->GetUserId());

    vtkSMTrace::StartTrace();
    tracer = vtkSMTrace::GetActiveTracer();
    tracer->SetPropertiesToTraceOnCreate(vtkSMTrace::RECORD_USER_MODIFIED_PROPERTIES);
    // smTrace always adds unneeded lines.
    // see smtrace.py::_start_trace_internal
    this->Internals->ExtraLines = QString(tracer->GetCurrentTrace().c_str());
  }

  this->updateAction();
}

//-----------------------------------------------------------------------------
void pqJupyterTraceReaction::sendToJupyter(QString trace)
{
  pqServer* server = pqApplicationCore::instance()->getActiveServer();
  pqApplicationCore* core = pqApplicationCore::instance();

  // Using pqCollaborationManager is kind of a trick here as a plugin cannot include `vtkSMMessage.h` when
  // building against an install of ParaView. One could directly use a vtkSMMessage and pqServer
  auto* manager = qobject_cast<pqCollaborationManager*>(core->manager("COLLABORATION_MANAGER"));
  if (server && manager)
  {
    manager->onChatMessage(server, manager->activeCollaborationManager()->GetUserId() , trace);
  }
}
