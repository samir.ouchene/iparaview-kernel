#include "pqJupyterCollaborationActions.h"

#include "pqJupyterTraceReaction.h"

#include <QApplication>
#include <QStyle>

//-----------------------------------------------------------------------------
pqJupyterCollaborationActions::pqJupyterCollaborationActions(QObject* p)
  : QActionGroup(p)
{
  QIcon icon = qApp->style()->standardIcon(QStyle::SP_DialogNoButton);
  QAction* traceAction = this->addAction(new QAction(icon, "Record Trace", this));
  new pqJupyterTraceReaction(traceAction);
}
